var Entity = require('../Entity.js');
var Damage = require('./Damage.js');

module.exports = function(x, y, socket) {
	this.entity = new Entity(x, y);
	this.move = 's';
	this.socket = socket
	
	this.equipmentChanged = false;
	
	var inventory = [];
	var equipped = {head: 0, body: 51, legs: 0, weapon: 0};
	
	var speed = 1.5
	var strength = 500;
	var facing = 'r';
	
	var inputLocked = false;
	var hitTimeOut;
	/* players update is ran at each frame. 
		Parameters: damages Array
	*/
	this.update = function(damages) {
		if(!inputLocked){
			if (this.move != 's') {
				if (this.move == 'l') {
					this.entity.deltaX = -speed;
					facing = 'l';
				}
				else if (this.move == 'r') {
					this.entity.deltaX = speed;
					facing = 'r';
				}
				else if (this.move == 'd') {
					this.entity.deltaY = speed;
					facing = 'd';
				}
				else if (this.move == 'u') {
					this.entity.deltaY = -speed;
					facing = 'u';
				}
				
				//DEBUG
				else if(this.move == 'i'){
					equipped.head = 50;
					this.equipmentChanged = true;
				}
				else if(this.move == 'o'){
					equipped.body = 51;
					this.equipmentChanged = true;
				}
				else if(this.move == 'p'){
					equipped.legs = 52;
					this.equipmentChanged = true;
				}
				else if(this.move == 'k'){
					
					equipped.head = 0;
					equipped.body = 0;
					equipped.legs = 0;
					this.equipmentChanged = true;
				}
				
				else if (this.move == 'h') {
					var tempDamage = this.getDamage();
					inputLocked = true;
					hitTimeOut = setTimeout(function() {
						damages.push(tempDamage);
						inputLocked = false;
					}, 500);
					
				}
				return true;
			}
		}
		

		return false;
	}

	this.collisionDetection = function(collidableArray, width, height) {

		// test section corners
		if (((this.entity.x + this.entity.deltaX) - (this.entity.size / 2)) < 0) {
			this.entity.deltaX = 0;
		}
		else if (((this.entity.x + this.entity.deltaX) + (this.entity.size / 2)) > width * this.entity.size) {
			this.entity.deltaX = 0;
		}
		else if (((this.entity.y + this.entity.deltaY) - (this.entity.size / 2)) < 0) {
			this.entity.deltaY = 0;
		}
		else if (((this.entity.y + this.entity.deltaY) + (this.entity.size / 2)) > height * this.entity.size) {
			this.entity.deltaY = 0;
		}

		// test tiles
		for (var i = 0; i < collidableArray.length; i++) {
			if (this.entity.testCollision(collidableArray[i].entity)) {
				this.entity.deltaX = 0;
				this.entity.deltaY = 0;
			}
		}
	}

	this.damageDetection = function(damages, collidableArray) {
		
		for (var i = 0; i < damages.length; i++) {
			var knockback = damages[i].damage;

			if (knockback > this.entity.size * 2 - 1) {
				knockback = this.entity.size * 2 - 1;
			}
			if (this.entity.testCollision(damages[i].entity)) {
				console.log("you hit: " + this.socket);
				switch (damages[i].direction) {
					case 'l':
						this.entity.x -= knockback;
						break;
					case 'r':
						this.entity.x += knockback;
						break;
					case 'u':
						this.entity.y -= knockback;
						break;
						//down
					default:
						this.entity.y += knockback;
				}

				var collidableObject = null;
				for (var j = 0; j < collidableArray.length; j++) {
					if (this.entity.testCollision(collidableArray[j].entity)) {
						collidableObject = collidableArray[j];
					}
				}

				if (collidableObject) {
					var emptySpace = false;

					while (!emptySpace) {
						
						if (this.entity.testCollision(collidableObject.entity)) {
							switch (damages[i].direction) {
								case 'l':
									this.entity.x += 1;
									break;
								case 'r':
									this.entity.x -= 1;
									break;
								case 'u':
									this.entity.y += 1;
									break;
									//down
								default:
									this.entity.y -= 1;
							}

						}
						
						else {
							emptySpace = true;
						}
					}
				}
				clearTimeout(hitTimeOut);
				inputLocked = false;
				return true;
			}
			return false;
		}
	}

	this.getDamage = function() {
		
		var position = {
			x: this.entity.x,
			y: this.entity.y
		};

		switch (facing) {
			case 'l':
				position.x -= (this.entity.size + 2);
				break;
			case 'r':
				position.x += (this.entity.size + 2);
				break;
			case 'u':
				position.y -= (this.entity.size + 2);
				break;
				//down
			default:
				position.y += (this.entity.size + 2);
		}

		var damage = new Damage(strength, position.x, position.y, facing, 2);
		damage.entity.size = 2;

		return damage;
	}
	
	this.getEquippedItems = function(){
		return equipped;
	}

}