var Entity = require('../Entity.js');

module.exports = function(damage, x, y, direction, ttl){
    
	this.entity = new Entity(x,y);
	this.damage = damage;
	this.direction = direction;
	this.ttl = ttl;
	
}