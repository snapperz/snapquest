var IdCounter = 0;

module.exports = function(x, y){
	this.x = x;
	this.y = y;
	this.id = IdCounter;
	IdCounter++;
	this.imageId = 0;
	
	this.deltaX = 0;
	this.deltaY = 0;
	
	this.size = 16;
	
	// test if collision with target !entity!.
	this.testCollision = function(target) {
			if( ((this.x + this.deltaX)+(this.size / 2)) > (target.x - (target.size / 2)) && 
			((this.x + this.deltaX)-(this.size / 2)) < (target.x + (target.size / 2)) &&
			((this.y + this.deltaY)+(this.size / 2)) > (target.y - (target.size / 2)) && 
			((this.y + this.deltaY)-(this.size / 2)) < (target.y + (target.size / 2))){
				return true;
			}
			else {
				return false;
			}
	}

}