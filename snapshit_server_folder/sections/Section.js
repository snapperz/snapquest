/*
	Section is a part of the whole world with its own logic and objects.
*/

var Tile = require('./objects/Tile.js');
var fs = require('fs');

var MAX_PLAYERS = 8;
var MAX_OBJECTS_PER_FRAME = 10;

module.exports = function(sectionID, io){

	// PRIVATE OBJECTLISTS
	// Contains all tiles in this section
	var tiles = [];
	//Contains all damageobjects in this section
	var damages = [];


	// PUBLIC OBJECTLISTS
	// Players are always sent to clients
	var players = [];

	// queue of objects that needs to be sent
	var objectQueue = [];
	var playerQueue = [];


	var width = 0;
	var height = 0;

	var OBJECT_SIZE = 16;

	this.id = sectionID;
	this.io = io;

	var objectX = 0;
	var objectY = 0;
	// read map file and push tiles accordingly. If map file was not found, generate 16x16 map with no tiles
	{
		var content="";
		try {
			content = fs.readFileSync('./maps/'+sectionID, 'utf8');
			if(process.platform == "linux") {
				width = content.split('\n')[0].length;
			} else {
				width = content.split('\n')[0].length-1;
			}
			
			
			content = content.replace(/(\r\n|\n|\r)/gm,"");
			height = content.length / width;

			
		} catch (err){
			
			if(content === null){
				for(var i = 0; i < 256; i++){
		    		content += '0';
		    	}
		    	height = 16;
		    	width = 16;
			}
		}

		i = 0;

		for(var y = 0; y < height; y++) {
			for(var x = 0; x < width; x++) {

				var c = content.charAt(i);
				if(c > 0) {
					var obj = new Tile(objectX, objectY);
					obj.entity.imageId = c;
					tiles.push(obj);
				}
				
				objectX += 16;
				i++;
			}
			objectX = 0;
			objectY += 16;
		}
		objectX = 0;
		objectY = 0;
		
	}



	/* 
		World uses this method to push player in this section
	 	Returns false if theres no room for new player
	*/
	this.pushPlayer = function(player){
		if(players.length < MAX_PLAYERS){
			player.equipmentChanged = true;
			pushPlayer(player);
			players.push(player);
			sendAllObjects(player);
			return true;
		}else{
			return false;
		}
	};

	var sendAllObjects = function(player) {
		console.log("Player connected and sending objects");
		players.forEach(function(object) {
			var playerEquipment = object.getEquippedItems();
			player.socket.emit('character', {x: object.entity.x, y: object.entity.y, imageId: object.entity.imageId, id: object.entity.id, equipment: playerEquipment});
		});
		tiles.forEach(function(object) {
			player.socket.emit('object', {x: object.entity.x, y: object.entity.y, imageId: object.entity.imageId, id: object.entity.id});
		});
		
		player.socket.emit('hero', {playerId: player.entity.id});
		
		console.log("Objects sent");

	};

	/* 
		Sends the next object in objectQueue to all players in this section. 
		Returns false if queue is empty
	*/
	var sendCount = 0;
	this.sendNextObject = function(){
		if(objectQueue.length === 0 || sendCount > MAX_OBJECTS_PER_FRAME){
			sendCount = 0;
			return false;

		}
		

		var object = objectQueue.shift();
		
		for(var i = 0 ; i < players.length ; i++){

			if(players[i].socket) {
				//players[i].socket.emit('debug', {x: object.x, y: object.y, imageId: object.imageId});
			} else {
				console.log('Meillä on haamupelaaja');
			}

		}
		sendCount++;
		return true;
	};
	/* 
		Sends the next player in playerQueue to all players in this section. 
		Returns false if queue is empty
	*/
	this.sendNextPlayer = function() {
		
		if(playerQueue.length === 0 || sendCount > MAX_OBJECTS_PER_FRAME){
			sendCount = 0;
			return false;
		}

		var object = playerQueue.shift();
		
		players.forEach(function(player){
			
			if(player.socket) {
				if(object.equipmentChanged){
					var playerEquipment = object.getEquippedItems();
					player.socket.emit('character', {x: object.entity.x, y: object.entity.y, imageId: object.entity.imageId, id: object.entity.id, equipment: playerEquipment});
					
				}else{
					player.socket.emit('character', {x: object.entity.x, y: object.entity.y, imageId: object.entity.imageId, id: object.entity.id});	
				}
				
			} else {
				console.log('Meillä on haamupelaaja');
			}
			
		});
		
		object.equipmentChanged = false;
		
		sendCount++;
		return true;
		
	};



	// Update logic of section. World calls this every frame.
	this.update = function(){

		//playerMovement
		for(var i = 0; i < players.length; i++) {
			if(players[i].damageDetection(damages, tiles)) {
				pushPlayer(players[i]);
			}
			// check if player has changes in its equipment and add him to the send queue
			if(players[i].equipmentChanged){
				pushPlayer(players[i]);
			}
			// Player specific update. Uses damage array internally (player checks himself if damage should be taken)
			if(players[i].update(damages)){
				players[i].collisionDetection(tiles, width, height);
				players[i].entity.x += players[i].entity.deltaX;
				players[i].entity.y += players[i].entity.deltaY;
				if(players[i].entity.deltaX !== 0 || players[i].entity.deltaY !== 0) {
					players[i].entity.deltaX = 0;
					players[i].entity.deltaY = 0;
					
					pushPlayer(players[i]);
				}
				
				
			}	

		}
		
		damages.forEach(function(d){
			d.ttl--;
			if(d.ttl <= 0){
				var i = damages.indexOf(d);
				damages.splice(i,1);
			}
		});
			
		
		while(this.sendNextPlayer());
		while(this.sendNextObject());

	};



	// call this for debug data of this section
	this.monitor = function() {
		console.log('----------');
		console.log('sektiossa: '+sectionID + '|Pelaajia: '+players.length+'kpl' +'|Jonossa objekteja: ' + playerQueue.length +'kpl');
	};

	var pushPlayer = function(player){
		var exists = false;

		for(var i = 0; i < playerQueue.length; i++){
			if(player.entity.id == playerQueue[i].entity.id){
				exists = true;
			}
		}

		if(!exists){
			playerQueue.push(player);
		}
	};

	this.removePlayer = function(player){
		var i = players.indexOf(player);
		var removedPlayer = players.splice(i,1);
		return removedPlayer;
	};

};
