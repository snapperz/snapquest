//custom modules
var Section = require('./sections/Section.js');
var Player = require('./sections/objects/Player.js');

/* WorldModule has all the sections 
	- Initializes sections
	- handles when sections are updated
	- gets objects from sections objectqueue passes them for socket.emit
*/

//socket connection functions
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var async = require('async');

var  WorldModule = function(){

	this.sectionList = [];
	var allPlayers = [];
	this.asyncUpdateSections = [];
// Create all the sections and put them in sectionList
	for(var y = 0; y < 2; y++){
		this.sectionList[y] = [];
		for(var x = 0; x < 2; x++){
			var id = 'section'+y+''+x;
			this.sectionList[y][x] = new Section(id, io);
			
		}
	}

	
	// Updates all the sections logic
	this.updateSections = function(){
		for(var y = 0; y < 2; y++){
			for(var x = 0; x < 2; x++){
				this.sectionList[y][x].update();
			}
		}
	}

	// Log debugstrings from all sections
	this.monitorSections = function() {
		console.log('*****************');
		for(var y = 0; y < 2; y++){
			for(var x = 0; x < 2; x++){
				this.sectionList[y][x].monitor();
			}
		}
		console.log('*****************');
	}


}

// Create a new (only) instance of world
var world = new WorldModule();
// Run all the sections updates periodically
setInterval(function(){world.updateSections()}, 30);

// DEGUG! Comment this out whenever possible. It can be costly
//setInterval(function(){world.monitorSections()}, 200);



app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});


// Socket specefic connection logic
io.on('connection', function(socket){

	var player = new Player(170,170,socket);

	world.sectionList[0][0].pushPlayer(player);

	socket.on('move', function(move){
		player.move = move.move;
	});

	socket.on('disconnect', function() {
		world.sectionList[0][0].removePlayer(player);
		console.log("player " + player.entity.id + " disconnected");
	});
 
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
