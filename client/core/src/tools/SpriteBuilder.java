package tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import gameObjects.GameObject;

public class SpriteBuilder {
	//static method to match objects imageID to right sprite / atlas
	public static void build(GameObject o){
		
		switch(o.imageId){
		
		case 1:
			 o.setAnimated(false);
			 o.textureAtlas = new TextureAtlas(Gdx.files.internal("atlases/wallPack.pack"), true );
			 o.sprite = new Sprite(o.textureAtlas.findRegion("wall"));
			break;
			
		default:
			 o.setAnimated(true);
			 o.origoY = -16;
			 o.origoX = -8;
			 o.textureAtlas = new TextureAtlas(Gdx.files.internal("atlases/"+o.imageId+".pack"), true );
			 System.out.println(o.imageId);
			break;
		}
	}
}
