package gameObjects;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class CharacterList {

	public static AtomicBoolean lock = new AtomicBoolean(false);
	public static GameObject hero = null;
	
	public static ArrayList<Character> characters = new ArrayList<Character>();
	
	public static void push(Character character, boolean equipmentChanged) {
			
			while(lock.getAndSet(true));
			boolean found = false;
			for(Character c : characters) {
				if(c.getSkeleton().getObjectId() == character.getSkeleton().getObjectId()) {
					found = true;
					if(equipmentChanged){
						
						if(character.getHead().imageId != c.getHead().imageId){
							c.setHead(character.getHead().imageId);
						}
						if(character.getBody().imageId != c.getBody().imageId){
							c.setBody(character.getBody().imageId);
						}
						if(character.getLegs().imageId != c.getLegs().imageId){
							c.setLegs(character.getLegs().imageId);
						}
					}
					c.getSkeleton().setPosition(character.getSkeleton().getPosition());
				}
			}
			if(!found) {
				characters.add(character);
			}
			lock.set(false);
		}
		
		public static void findHero(int id) {
			while(lock.getAndSet(true));
			for(Character c : characters) {
				if(c.getSkeleton().getObjectId() == id) {
					hero = c.getSkeleton();
				}
			}
			lock.set(false);
		}
}
