package gameObjects;


import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class ObjectList {
	
	public static AtomicBoolean lock = new AtomicBoolean(false);
	
	public static ArrayList<GameObject> objects = new ArrayList<GameObject>();
	// If gameobject exists in list replace it. If not, push it
	public static void push(GameObject obj) {
		
		while(lock.getAndSet(true));
		boolean found = false;
		for(GameObject o : objects) {
			if(o.getObjectId() == obj.getObjectId()) {
				found = true;
				o.setPosition(obj.getPosition());
			}
		}
		if(!found) {
			objects.add(obj);
		}
		lock.set(false);
	}	
	
	
	
}
