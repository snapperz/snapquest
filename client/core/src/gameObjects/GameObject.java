package gameObjects;

import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import screens.GameScreen;
import tween.Interpolate;

import aurelienribon.tweenengine.Tween;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

public class GameObject implements Comparable<GameObject>{
	private Vector2 position;
	public Sprite sprite;
	private int objectID;
	public int imageId = -1;
	
	private float animationTime = 0;
	
	public TextureAtlas textureAtlas;
	private Animation stillLeft, stillRight, stillFront, stillBack, moveLeft, moveRight, moveUp, moveDown;

	private Random r = new Random();
	private float random;
	
	private char lastDirection = 'd';
	
	private Boolean animated = false;
	public int origoY = 0;
	public int origoX = 0;
	
	public boolean isEquipped = false;
	// tolerance between sprite x and real x when animating
	private float animationTolerance = 2f;
	
	
	
	public GameObject(JSONObject obj){
		
			try {
				
				this.position = new Vector2(Float.parseFloat(obj.getString("x")), Float.parseFloat(obj.getString("y")));
				this.objectID = Integer.parseInt(obj.getString("id"));
				this.imageId = Integer.parseInt(obj.getString("imageId"));
				
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		Tween.registerAccessor(Sprite.class, new Interpolate());
	}
	
	
	public GameObject(Vector2 position, int imageId){
		this.position = position;
		this.imageId = imageId;
		this.isEquipped = true;
	}
	
	public GameObject(){
		isEquipped = false;
	}
	
	public void initAnimations(){
		
		stillLeft = new Animation(1/2f, textureAtlas.findRegions("stillLeft"));
		stillRight = new Animation(1/2f, textureAtlas.findRegions("stillRight"));
		stillFront = new Animation(1/2f, textureAtlas.findRegions("stillFront"));
		stillBack = new Animation(1/2f, textureAtlas.findRegions("stillBack"));
		
		moveLeft = new Animation(1/5f, textureAtlas.findRegions("moveLeft"));
		moveRight = new Animation(1/5f, textureAtlas.findRegions("moveRight"));
		moveUp = new Animation(1/5f, textureAtlas.findRegions("moveUp"));
		moveDown = new Animation(1/5f, textureAtlas.findRegions("moveDown"));
		
		stillLeft.setPlayMode(PlayMode.LOOP);
		stillRight.setPlayMode(PlayMode.LOOP);
		stillFront.setPlayMode(PlayMode.LOOP);
		stillBack.setPlayMode(PlayMode.LOOP);
		
		moveLeft.setPlayMode(PlayMode.LOOP);
		moveRight.setPlayMode(PlayMode.LOOP);
		moveUp.setPlayMode(PlayMode.LOOP);
		moveDown.setPlayMode(PlayMode.LOOP);
		
		random = r.nextFloat();
		
		sprite = new Sprite(stillLeft.getKeyFrame(random));
	}
	
	public void animate(float delta, SpriteBatch batch){
		animationTime += delta;
				
		Vector2 spritePos = new Vector2(Math.round(sprite.getX()), Math.round(sprite.getY()));
		Vector2 realPos = new Vector2(Math.round(position.x), Math.round(position.y));
		if(spritePos.x > realPos.x + animationTolerance){
			sprite.setRegion(moveLeft.getKeyFrame(animationTime));
			lastDirection = 'l';
		}
		else if(spritePos.x < realPos.x - animationTolerance){
			sprite.setRegion(moveRight.getKeyFrame(animationTime));
			lastDirection = 'r';
		}
		else if(spritePos.y>realPos.y + animationTolerance){
			sprite.setRegion(moveUp.getKeyFrame(animationTime));
			lastDirection = 'u';
		}
		else if(spritePos.y < realPos.y - animationTolerance){
			sprite.setRegion(moveDown.getKeyFrame(animationTime));
			lastDirection = 'd';
		}
		else{
			switch(lastDirection) {
				case 'd':
					sprite.setRegion(stillFront.getKeyFrame(animationTime));
					break;
				case 'u':
					sprite.setRegion(stillBack.getKeyFrame(animationTime));
					//sprite.setRegion(stillFront.getKeyFrame(animationTime));
					break;
				case 'l':
					sprite.setRegion(stillLeft.getKeyFrame(animationTime));
					//sprite.setRegion(stillFront.getKeyFrame(animationTime));
					break;
				case 'r':
					sprite.setRegion(stillRight.getKeyFrame(animationTime));
					//sprite.setRegion(stillFront.getKeyFrame(animationTime));
					break;
				default:
					//sprite.setRegion(stillFront.getKeyFrame(animationTime));
					sprite.setRegion(stillFront.getKeyFrame(animationTime));
					break;
			}
			
		}

		this.sprite.setY(this.sprite.getY() + origoY);
		this.sprite.setX(this.sprite.getX() + origoX);
		this.sprite.draw(batch);
	}
	
	public void interpolateSpritePosition() {
		Tween.to(this.sprite, Interpolate.POSITION, 0.1f).target(position.x, position.y).start(GameScreen.getTween());

	}
	
	//GETTERS AND SETTERS
	public Vector2 getPosition(){
		return position;
	}
	
	public void setPosition(Vector2 position){
		this.position = position;
	}
	
	public void setX(float x){
		this.position.x = x;
	}
	
	public void setY(float y){
		this.position.y = y;
	}
	
	public Sprite getSprite(){
		return sprite;
	}
	
	public int getObjectId(){
		return objectID;
	}
	
	public void setObjectId(int objectID){
		this.objectID = objectID;
	}

	public Boolean getAnimated() {
		return animated;
	}

	public void setAnimated(Boolean isAnimated) {
		this.animated = isAnimated;
	}

	@Override
	public int compareTo(GameObject compareObject) {
		return (this.position.y - compareObject.getPosition().y) > 0 ? 1:-1;
	}
}
