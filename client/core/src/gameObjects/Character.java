package gameObjects;

import org.json.JSONException;
import org.json.JSONObject;

import tools.SpriteBuilder;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;



public class Character implements Comparable<Character>{
	
	private GameObject skeleton;
	
	private GameObject head, body, legs;
	private JSONObject equipment;
	
	public Character(JSONObject obj){
		
		skeleton = new GameObject(obj);
		
		if(obj.has("equipment")){
			
			try {
				equipment = obj.getJSONObject("equipment");
				
				if(Integer.parseInt(equipment.getString("head")) > 0){
					this.head = new GameObject(skeleton.getPosition(), Integer.parseInt(equipment.getString("head")));
					this.head.isEquipped = true;
				}else{
					this.head = new GameObject();
				}
				
				if(Integer.parseInt(equipment.getString("body")) > 0){
					this.body = new GameObject(skeleton.getPosition(), Integer.parseInt(equipment.getString("body")));
					this.body.isEquipped = true;
				}else{
					this.body = new GameObject();
				}
				
				if(Integer.parseInt(equipment.getString("legs")) > 0){
					this.legs = new GameObject(skeleton.getPosition(), Integer.parseInt(equipment.getString("legs")));
					this.legs.isEquipped = true;
				}else{
					this.legs = new GameObject(); 
				}
				
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			
			this.head = new GameObject();
			this.body = new GameObject(); 
			this.legs = new GameObject(); 
		}
		
	}
	
	public void gearChanged(JSONObject obj){
		
		if(obj.has("equipment")){
					
			try {
				equipment = obj.getJSONObject("equipment");
				
				if(Integer.parseInt(equipment.getString("head")) > 0){
					this.head = new GameObject(skeleton.getPosition(), Integer.parseInt(equipment.getString("head")));
					this.head.isEquipped = true;
				}else{
					this.head = new GameObject();
				}
				
				if(Integer.parseInt(equipment.getString("body")) > 0){
					this.body = new GameObject(skeleton.getPosition(), Integer.parseInt(equipment.getString("body")));
					this.body.isEquipped = true;
				}else{
					this.body = new GameObject();
				}
				
				if(Integer.parseInt(equipment.getString("legs")) > 0){
					this.legs = new GameObject(skeleton.getPosition(), Integer.parseInt(equipment.getString("legs")));
					this.legs.isEquipped = true;
				}else{
					this.legs = new GameObject(); 
				}
				
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			
			this.head = new GameObject();
			this.body = new GameObject(); 
			this.legs = new GameObject(); 
		}
		
	}
	
	public void initCharacterItems(){
		
		if(head.isEquipped){
			SpriteBuilder.build(head);
			head.initAnimations();
		}
		
		if(body.isEquipped){
			SpriteBuilder.build(body);
			body.initAnimations();
		}
		
		if(legs.isEquipped){
			SpriteBuilder.build(legs);
			legs.initAnimations();
		}	
		
	}
	
	public void updateItems(SpriteBatch batch, float delta){
		if(legs.isEquipped){
			
			legs.setPosition(skeleton.getPosition());
			legs.interpolateSpritePosition();
			legs.animate(delta, batch);
			
		}
		
		if(body.isEquipped){
			
			body.setPosition(skeleton.getPosition());
			body.interpolateSpritePosition();
			body.animate(delta, batch);
		}
		
		if(head.isEquipped){
			
			head.setPosition(skeleton.getPosition());
			head.interpolateSpritePosition();
			head.animate(delta, batch);
		}
		
	}
	
	public GameObject getHead(){
		return head;
	}
	
	public GameObject getBody(){
		return body;
	}
	
	public GameObject getLegs(){
		return legs;
	}
	
	public void setHead(int imageId){
		if(imageId > 0){
			System.out.println("k�ytiin t��ll�");
			head.imageId = imageId;
			head.textureAtlas = null;
			head.isEquipped = true;
		}else{
			head = new GameObject();
		}
	}
	
	public void setBody(int imageId){
		if(imageId > 0){
			body.imageId = imageId;
			body.textureAtlas = null;
			body.isEquipped = true;
		}else{
			body = new GameObject();
		}
	}
	
	public void setLegs(int imageId){
		if(imageId > 0){
			legs.imageId = imageId;
			legs.textureAtlas = null;
			legs.isEquipped = true;
		}else{
			legs = new GameObject();
		}
	}
	
	
	public GameObject getSkeleton(){
		return skeleton;
	}
	
	@Override
	public int compareTo(Character compareObject) {
		return (this.skeleton.getPosition().y - compareObject.getSkeleton().getPosition().y) > 0 ? 1:-1;
	}
}
