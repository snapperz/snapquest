package com.ohsnap2014.game;

import gameObjects.CharacterList;
import gameObjects.GameObject;
import gameObjects.ObjectList;
import gameObjects.Character;

import java.net.URISyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

public class Connection implements Runnable {
	public static Socket socket = null;
	public static String host = null;
	public static int connectionID = 0;
	
	@Override
	public void run() {
		if (host != null) {
		try {
			socket = IO.socket("http://" + host + ":3000");
			
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		// Emitter listener
		socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
			
		  @Override
		  public void call(Object... args) {
			  
		  }

		}).on("object", new Emitter.Listener() {

		  @Override
		  public void call(Object... args) {
			  JSONObject obj = (JSONObject)args[0];
			  GameObject gameObject = new GameObject(obj);
			  //System.out.println(gameObject.getPosition());
			  ObjectList.push(gameObject);
		  }

		}).on("character", new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				  JSONObject obj = (JSONObject)args[0];
				  //System.out.println(obj);
				  Character character = new Character(obj);
				  CharacterList.push(character, obj.has("equipment"));
			  }

			}).on("hero", new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				  JSONObject obj = (JSONObject)args[0];
				  try {
					connectionID = Integer.parseInt(obj.getString("playerId"));
					CharacterList.findHero(connectionID);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			  }

		}).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

		  @Override
		  public void call(Object... args) {
			  // disconnect event
		  }

		}).on(Socket.EVENT_ERROR, new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				  JSONObject obj = (JSONObject)args[0];
				  System.out.println(obj);
			  }

			});
		// /Emitter listener
		socket.connect();
		} else {
			System.out.println("Fuck you noob");
		}
	}

}
