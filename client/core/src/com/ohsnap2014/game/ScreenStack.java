package com.ohsnap2014.game;
import screens.MainMenuScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;

public class ScreenStack {

	private static Game game;
	
	public static void init(Game gaem) {
		game = gaem;
		game.setScreen(new MainMenuScreen());
	}

	public static void setScreen(Screen screen) {
		game.setScreen(screen);
	}
}
