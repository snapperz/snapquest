package playerFeedback;

import org.json.JSONException;
import org.json.JSONObject;

import com.ohsnap2014.game.Connection;

public class Movement {
	private static char key  = 's';
	
	public static void changeKey(char newKey) throws JSONException{
		if(key != newKey){
			key = newKey;
			JSONObject obj = new JSONObject();
			obj.put("move", ""+key);
			System.out.println(key);
			Connection.socket.emit("move", obj);
		}
	}
	
}
