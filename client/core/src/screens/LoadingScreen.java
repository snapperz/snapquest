package screens;

import tools.SpriteBuilder;
import gameObjects.CharacterList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.ohsnap2014.game.ScreenStack;

public class LoadingScreen implements Screen {
	private static final int LOADING_SCREEN_WIDTH = 480;
	private static final int LOADING_SCREEN_HEIGHT = 320;
	
	private Screen nextScreen = null;
	public TextureAtlas loadingAtlas;
	private Animation loading;
	private Sprite loadingSprite;
	private float animationTime;
	
	private OrthographicCamera camera;
	private SpriteBatch batch;
	
	public LoadingScreen(Screen nextScreen) {
		this.nextScreen = nextScreen;
	}
	
	@Override
	public void render(float delta) {
		GL20 gl = Gdx.gl20;
		gl.glClearColor(0, 0, 0, 0);
		gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
			loadingAnimation(batch, delta);
		batch.end();
		
		if(CharacterList.hero != null) {
			
			SpriteBuilder.build(CharacterList.hero);
			CharacterList.hero.initAnimations();
			System.out.println(CharacterList.hero.sprite);
			ScreenStack.setScreen(nextScreen);
			
		}
		
	}

	@Override
	public void resize(int width, int height) {
		/* TODO Auto-generated method stub*/
		
	}
	
	private void initLoadingAnimation(){
		loadingAtlas = new TextureAtlas(Gdx.files.internal("atlases/loadingAtlas.pack"), true);
		loading = new Animation(1/5f, loadingAtlas.findRegions("loading"));
		loading.setPlayMode(PlayMode.LOOP);
		loadingSprite = new Sprite(loading.getKeyFrame(0));
		loadingSprite.flip(false, true);
	}
	
	private void loadingAnimation(SpriteBatch batch, float delta){
		animationTime += delta;
		loadingSprite.setRegion(loading.getKeyFrame(animationTime));
		batch.draw(loadingSprite, LOADING_SCREEN_WIDTH / 2, LOADING_SCREEN_HEIGHT / 2);
	}
	
	@Override
	public void show() {
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		camera.setToOrtho(true, LOADING_SCREEN_WIDTH, LOADING_SCREEN_HEIGHT);
		initLoadingAnimation();
	}

	@Override
	public void hide() { /* TODO Auto-generated method stub*/ }

	@Override
	public void pause() { /* TODO Auto-generated method stub*/ }

	@Override
	public void resume() { /* TODO Auto-generated method stub*/ }

	@Override
	public void dispose() { /* TODO Auto-generated method stub*/ }

}
