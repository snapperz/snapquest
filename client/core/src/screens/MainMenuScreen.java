package screens;
//SOCKET IO IMPORTS
//----------------


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.ohsnap2014.game.Connection;
import com.ohsnap2014.game.ScreenStack;



public class MainMenuScreen implements Screen {
	private static final int MAINMENU_WIDTH = 480;
	private static final int MAINMENU_HEIGHT = 320;
	private SpriteBatch batch;
	private OrthographicCamera camera;
	
	private Stage stage;
	private Table table;
	private TextureAtlas atlas;
	private Skin buttonSkin, textBoxSkin;
	
	private TextButton connect;
	private TextField hostName;
	private FitViewport viewPort;
	
	@Override
	public void render(float delta) {
		GL20 gl = Gdx.gl20;
		gl.glClearColor(0, 0, 0, 0);
		gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		batch.begin();
			stage.act();
			stage.draw();
		batch.end();
	}
	
	@Override
	public void show() {
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		camera.setToOrtho(false);
		viewPort = new FitViewport(MAINMENU_WIDTH, MAINMENU_HEIGHT, camera);
		stage = new Stage(viewPort);
		stage.getViewport().setCamera(camera);
		table = new Table();
		table.setFillParent(true);
		initMenuComponents();
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void resize(int width, int height) {
		
	}
	
	private void initMenuComponents(){
		atlas = new TextureAtlas(Gdx.files.internal("atlases/menuAtlas.pack"));
		buttonSkin = new Skin(Gdx.files.internal("jsons/button.json"), atlas);
		textBoxSkin = new Skin(Gdx.files.internal("jsons/textBox.json"), atlas);
		
		connect = new TextButton("connect", buttonSkin);
		hostName = new TextField("ebuntu.dy.fi", textBoxSkin);
		addListeners();
		
		table.add(hostName).padBottom(25).width(250);
		table.row();
		table.add(connect).width(250);
		stage.addActor(table);
	}
	
	private void addListeners(){
		connect.addListener(new InputListener(){
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){	
				return true;
			}
			public void touchUp(InputEvent event, float x, float y, int pointer, int button){
				Connection.host = hostName.getText();
				(new Thread(new Connection())).start();
				ScreenStack.setScreen(new LoadingScreen(new GameScreen()));
			}
		});
	}

	@Override
	public void hide() { }

	@Override
	public void pause() { }

	@Override
	public void resume() { }

	@Override
	public void dispose() {
		stage.dispose();
	}
}
