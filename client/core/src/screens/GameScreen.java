package screens;


import java.util.Collections;

import org.json.JSONException;

import playerFeedback.Movement;
import tools.SpriteBuilder;
import gameObjects.CharacterList;
import gameObjects.GameObject;
import gameObjects.ObjectList;
import gameObjects.Character;
import aurelienribon.tweenengine.TweenManager;
import buttons.Joystick;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;



public class GameScreen implements Screen  {
	private static final int CAMERA_VIEWPORT_WIDTH = 320;
	private static final int CAMERA_VIEWPORT_HEIGHT = 240;
	
	private static TweenManager tweenManager;
	
	private OrthographicCamera camera;
	private SpriteBatch batch;
	
	private Joystick joystick;
	private Stage stage;
	
	public GameScreen(){
		joystick = new Joystick();
	}
	
	@Override
	public void render(float delta) {
		GL20 gl = Gdx.gl20;
		gl.glClearColor(0, 0, 0, 0);
		gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		tweenManager.update(Gdx.graphics.getDeltaTime());
		
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		
			while(ObjectList.lock.getAndSet(true));
			
			Collections.sort(ObjectList.objects);
			
			for(GameObject o: ObjectList.objects) {
				
				if(o.textureAtlas == null) {
					
					SpriteBuilder.build(o);
					
					if(o.getAnimated()){
						o.initAnimations();
					}
				}

				if(o.getAnimated()){
					o.animate(delta, batch);
				}else{
					o.sprite.draw(batch);
				}
				o.interpolateSpritePosition();
			}
			
			ObjectList.lock.set(false);
			
			while(CharacterList.lock.getAndSet(true));
			
				Collections.sort(CharacterList.characters);
				
				for(Character c : CharacterList.characters){
					
					if(c.getSkeleton().textureAtlas == null) {
						SpriteBuilder.build(c.getSkeleton());
						c.getSkeleton().initAnimations();
						
					}
					
					
					if((c.getHead().textureAtlas == null && c.getHead().isEquipped) 
					|| (c.getBody().textureAtlas == null && c.getBody().isEquipped)
					|| (c.getLegs().textureAtlas == null && c.getLegs().isEquipped)){
						
						c.initCharacterItems();
					}
					
					
						
				
					c.getSkeleton().interpolateSpritePosition();
					c.getSkeleton().animate(delta, batch);
					c.updateItems(batch, delta);
				}
				
			
			
			camera.position.lerp(new Vector3(CharacterList.hero.getPosition().x,CharacterList.hero.getPosition().y ,0), 0.1f) ;
			camera.update();
			
			CharacterList.lock.set(false);
			
			
		batch.end();
		try {
			playerMovement();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
        /*
         * Touchpad handling
         * 
         * If left side of the screen is touched draw touchpads knob to touched location
         * and manually set it to touch the knob.
         */
		Vector3 v3 = new Vector3(stage.stageToScreenCoordinates(new Vector2(Gdx.input.getX(), Gdx.input.getY())), 0);
        if(Gdx.input.justTouched() && Gdx.input.getX() < Gdx.graphics.getWidth()/2){  	
        	stage.addTouchFocus(joystick.touchpad.getListeners().first(), joystick.touchpad, joystick.touchpad, 0, 0);
        	joystick.touchpad.setPosition(v3.x - joystick.touchpad.getWidth()/2, v3.y - joystick.touchpad.getHeight()/2);
        }
        if (Gdx.input.isTouched() && joystick.isEnabled() && Gdx.input.getX() < Gdx.graphics.getWidth()/2) {
   
        	this.stage.act(Gdx.graphics.getDeltaTime());
            this.stage.draw();
        }	
	}

	@Override
	public void show() {
		batch = new SpriteBatch();
		camera = new OrthographicCamera(CAMERA_VIEWPORT_WIDTH, CAMERA_VIEWPORT_HEIGHT);
		camera.setToOrtho(true, CAMERA_VIEWPORT_WIDTH, CAMERA_VIEWPORT_HEIGHT);
		
		camera.position.x = CharacterList.hero.sprite.getX() + CharacterList.hero.sprite.getOriginX();
		camera.position.y = CharacterList.hero.sprite.getY() + CharacterList.hero.sprite.getOriginY();
		tweenManager = new TweenManager();
		//Create a Stage and add TouchPad
        this.stage = new Stage();
        this.stage.addActor(joystick.touchpad);
        Gdx.input.setInputProcessor(stage);
        
	}
	
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
		batch.dispose();
	}
	
	public static TweenManager getTween() {
		return tweenManager;
	}
	private void playerMovement() throws JSONException{
		
		if(Gdx.input.isKeyPressed(Keys.SPACE)) {
			Movement.changeKey('h');
		}
		else if(Gdx.input.isKeyPressed(Keys.W) || joystick.touchpad.getKnobPercentY() > 0.5f){
			Movement.changeKey('u');
		}
		else if(Gdx.input.isKeyPressed(Keys.S) || joystick.touchpad.getKnobPercentY() < -0.5f){
			Movement.changeKey('d');
		}
		else if(Gdx.input.isKeyPressed(Keys.A) || joystick.touchpad.getKnobPercentX() < -0.5f){
			Movement.changeKey('l');
		}
		else if(Gdx.input.isKeyPressed(Keys.D) || joystick.touchpad.getKnobPercentX() > 0.5f){
			Movement.changeKey('r');
		} 
		
		
		else if(Gdx.input.isKeyPressed(Keys.I)){
			Movement.changeKey('i');
		}
		else if(Gdx.input.isKeyPressed(Keys.O)){
			Movement.changeKey('o');
		}
		else if(Gdx.input.isKeyPressed(Keys.P)){
			Movement.changeKey('p');
		}
		else if(Gdx.input.isKeyPressed(Keys.K)){
			Movement.changeKey('k');
		}
		
		else if(Gdx.input.isKeyPressed(Keys.BACK)) {
			System.exit(0);
		}
		else{
			Movement.changeKey('s');
		}
		
	}
}
