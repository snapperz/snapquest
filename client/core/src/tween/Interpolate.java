package tween;

import aurelienribon.tweenengine.TweenAccessor;

import com.badlogic.gdx.graphics.g2d.Sprite;


public class Interpolate implements TweenAccessor<Sprite> {

	public static final int POSITION = 1;
	
	@Override
	public int getValues(Sprite target, int tweenType, float[] returnValues) {
		switch(tweenType) {
		case POSITION:
			returnValues[0] = target.getX();
			returnValues[1] = target.getY();
			return 2;
			
			default:
				assert false;
				return -1;
		}
	}

	@Override
	public void setValues(Sprite target, int tweenType, float[] newValues) {
		switch(tweenType) {
		case POSITION:
			target.setPosition(newValues[0], newValues[1]);
			break;
		default:
			assert false;
			break;
		}
		
	}
	
}
