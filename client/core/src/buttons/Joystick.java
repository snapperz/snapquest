package buttons;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

/**
 * Created by @author Kami on 11.10.2014.
 * 
 * TODO: For simplicity move every function for touchpad to joystick and
 * remove getTouchpad method. This way you don't get confused whether to use touchpad or joystick.
 * ...or extend Touchpad?
 */
public class Joystick {
    public Touchpad touchpad;
    private Touchpad.TouchpadStyle touchpadStyle;

    private Skin touchpadSkin;

    private Drawable touchBackground;
    private Drawable touchKnob;

    /**
     * Create new Joystick
     */
    public Joystick(){
        this.touchpadSkin = new Skin();
        this.touchpadSkin.add("touchBackground", new Texture("ui/touchBackground.png"));
        this.touchpadSkin.add("touchKnob", new Texture("ui/touchKnob.png"));
        
        this.touchBackground = touchpadSkin.getDrawable("touchBackground");
        this.touchKnob = touchpadSkin.getDrawable("touchKnob");
        this.touchKnob.setMinHeight(50);
        this.touchKnob.setMinWidth(50);
        
        this.touchpadStyle = new Touchpad.TouchpadStyle();
        this.touchpadStyle.background = touchBackground;
        this.touchpadStyle.knob = touchKnob;

        this.touchpad = new Touchpad(10, touchpadStyle);
        this.touchpad.setBounds(15, 15, 100, 100);   
    }
    
    /**
     * Set visibility of joystick
     * @param isEnabled
     */
    public void setEnabled(boolean isEnabled){
    	this.touchpad.setVisible(isEnabled ? true : false);
    }
    
    /**
     * Return visibility of joystick
     * @return
     */
    public boolean isEnabled() { return this.touchpad.isVisible(); }
    
    /**
     * Return touchpad
     * @return
     */
    public Touchpad getTouchPad(){ return this.touchpad; }
}
